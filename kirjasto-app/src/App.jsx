import { useState, useEffect } from 'react'
import './App.css'
import Banner from './Banner'
import LeftColumn from './LeftColumn'
import MainColumn from './MainColumn'
import RightColumn from './RightColumn'
import Footer from './Footer'
import axios from 'axios'
import Reviews from './Reviews'
import LoginWindow from './LoginWindow'
import RegisterWindow from './RegisterWindow'
import UserInfoWindow from './UserInfoWindow'

function App() {

  const [books, setBooks] = useState([])
  const [reviews, setReviews] = useState([])
  const [counter, setCounter] = useState(0)
  const [booksOrReviews, setBooksOrReviews] = useState('books')
  const [username, setUsername] = useState("")
  const [password, setPassword] = useState("")
  const [isLoggedIn, setIsLoggedIn] = useState(false)
  const [filteredText, setFilteredText] = useState('')
  const [token, setToken] = useState('')
  const [genre, setGenre] = useState('All')
  const [minValue, set_minValue] = useState(1900)
  const [maxValue, set_maxValue] = useState(2024)
  const [refreshMyBooks, setRefreshMyBooks] = useState(false)
  const [booksToShow, setBooksToShow] = useState(1000)
  const [recommendBook, setRecommendBook] = useState([])
  const [recommendBookIndex, setRecommendBookIndex] = useState(1)
  const [recommendArr, setRecommendArr] = useState([])
  const [myLoans, setMyLoans] = useState([])
  const [readMyReviews, setReadMyReviews] = useState(false)
  const [fees, setFees] = useState(0)
  const [lateLoans, setLateLoans] = useState(0)
  const [currentLoans, setCurrentLoans] = useState(0)
  const [totalLoans, setTotalLoans] = useState(0)
  const [favoriteGenre, setFavoriteGenre] = useState('')
  const [bannerDisplay, setBannerDisplay] = useState(true)
  const [errorMessage, setErrorMessage] = useState('')
  const [width, setWidth] = useState(window.innerWidth);
  const [select, setSelect] = useState('byAuthor')
  const [browseBy, setbrowseBy] = useState(false)


  //UseEffect joka tallettaa näytön leveyden tilamuuttujaan
  useEffect(() => {
   const handleResizeWindow = () => setWidth(window.innerWidth);
    // subscribe to window resize event "onComponentDidMount"
    window.addEventListener("resize", handleResizeWindow);
    return () => {
      // unsubscribe "onComponentDestroy"
      window.removeEventListener("resize", handleResizeWindow);
    };
  }, []);

  //Haetaan kaikki kirjat 
  useEffect(() => {
    getAllBooks()
    getAllReviews()
  }, [])

  //Haetaan arvostelut jos on annettu uusia arvosteluja
  useEffect(() => {
    getAllReviews()
  }, [refreshMyBooks, readMyReviews])

  //Haetaan keskiarvot uudestaan jos arvostelut lista on muuttunut
  useEffect(() => {
    averageForAllBooks()
  }, [books, reviews])

    //Tyhjennetään filtterit uloskirjauksessa
    useEffect(() => {
      if(isLoggedIn === false){
        setFilteredText('')
        setGenre('All')
        setCounter(0)
        set_minValue(1900)
        set_maxValue(2024)
        setSelect('ByAuthor')
      }
    }, [isLoggedIn])

  //Haetaan suositeltu kirja kun käyttäjä kirjautuu, seuraa myös jos kirjalista päivittyy tai arvostelut päivttyy
  useEffect(() => {
    if (books.length > 0 && reviews.length > 0) {
      getRecommendation()
    }
  }, [reviews, books, isLoggedIn, myLoans])

  //Funktio joka hakee kaikki kirja. Oletuksena keskiarvoksi asetetaan 0. Jos author on nulla asetetaan authoriksi unknown
  const getAllBooks = async () => {
    const url = 'https://buutti-library-api.azurewebsites.net/api/books'
    const response = await axios.get(url)
    response.data.map(book => book.average = 0)
    response.data.map(book => book.author === null ? book.author = "Unknown" : book.author = book.author)
    response.data.sort(
      (a, b) =>
        (a.author.split(' ')[1] > b.author.split(' ')[1]) ? 1 : (a.author.split(' ')[1] < b.author.split(' ')[1]) ? -1 : 0)
    setBooks(response.data)
  }

  //Haetaan kaikki arvostelut. Sortataan uusine arvostelu ensimmmäiseksi listalle
  const getAllReviews = async () => {
    const url = 'https://buutti-library-api.azurewebsites.net/api/reviews'
    const response = await axios.get(url)
    response.data.sort(
      (a, b) =>
        (a.id < b.id) ? 1 : (a.id > b.id) ? -1 : 0)
    setReviews(response.data)
  }

  //Funktio joka mäppää kirjat ja kutsuu countAverage funktiota
  function averageForAllBooks() {
    {
      books.map(book => reviews.find(review => review.book_isbn === book.isbn)
        ?
        countAverage(reviews.filter(review => review.book_isbn === book.isbn))
        :
        null
      )
    }
  }

  //Funktio joka laskee ja asettaa keskiarvot kirjoille
  function countAverage(averageArr) {
    let averageOfReviews = 0
    for (let i = 0; i < averageArr.length; i++) {
      averageOfReviews = averageOfReviews + averageArr[i].rating
    }
    averageOfReviews = averageOfReviews / averageArr.length
    books.find(book => book.isbn === averageArr[0].book_isbn ? book.average = averageOfReviews : null)
  }

  //Funktio joka hakee kirjasuosituksen, käyttäjän tykkäämän genren mukaan
  function getRecommendation() {
    //Hateaan käyttäjän tekemät arvostelut
    let myReviews = reviews.filter(review => review.username === username)
    if (myReviews[0] !== undefined && books.length > 0) {
      //Etsitään omista arvosteluista paras arvostelu
      myReviews.sort(
        (a, b) =>
          (a.rating < b.rating) ? 1 : (a.rating > b.rating) ? -1 : 0)
      //Etsitään parhaan arvostelun genre
      let myFavoriteGenre = books.find(book => book.isbn === myReviews[0].book_isbn)
      //Genre tilamuuttujaan
      setFavoriteGenre(myFavoriteGenre.genre)
      //Etsitään kirjoista kyseisen genren kirjat
      let listOfRecommedations = books.filter(book => book.genre === myFavoriteGenre.genre)
      //Etsitään paras kirja suositeltavaksi käyttäjälle kyseisestä genrestä
      listOfRecommedations.sort(
        (a, b) =>
          (a.average < b.average) ? 1 : (a.average > b.average) ? -1 : 0)
      //Poistetaan suositus listalta kirjat, jotka käyttäjä on jo lainannut
      for (let i = 0; i < listOfRecommedations.length; i++) {
        for (let j = 0; j < myLoans.length; j++)
          if (listOfRecommedations[i].isbn === myLoans[j].book_isbn) {
            listOfRecommedations.splice(i, 1)
            i--
            break
          }
      }
      //Suosituksiin max 5 kirjaa
      listOfRecommedations.splice(5)
      setRecommendArr(listOfRecommedations)
      setRecommendBook(listOfRecommedations[0])
    }
    //Jos käyttäjä ei ole antanut arvosteluja, suositellaan kaikista genreistä kirjaa jolla on paras keskiarvo
    else {
      let listOfRecommedations = [...books]
      listOfRecommedations.sort(
        (a, b) =>
          (a.average < b.average) ? 1 : (a.average > b.average) ? -1 : 0)
        //Poistetaan suositus listalta kirjat, jotka käyttäjä on jo lainannut
      for (let i = 0; i < listOfRecommedations.length; i++) {
        for (let j = 0; j < myLoans.length; j++)
          if (listOfRecommedations[i].isbn === myLoans[j].book_isbn) {
            listOfRecommedations.splice(i, 1)
            i--
            break
          }
      }
      //Suosituksiin max 5 kirjaa
      listOfRecommedations.splice(5)
      setRecommendArr(listOfRecommedations)
      setRecommendBook(listOfRecommedations[0])
      setFavoriteGenre('')
    }
  }

  useEffect(() => {
    getLateLoans()
  }, [myLoans])

  //funktio joka hakee myöhässä olevien lainojen määrän (ja päivittää sakkomaksun summan sen mukaisesti)
  function getLateLoans() {
    let lateLoansCounter = 0
    const dateToday = new Date().getTime()
    myLoans.map(item => {
      if (dateToday > item.due && item.returned === null) {
        lateLoansCounter++
      }
    })
    setLateLoans(lateLoansCounter)
    setFees(lateLoansCounter * 2.5)
  }

  return (
    <div className='App'>
      <div className='row'>
        <div className='HeaderInfo col-8'></div>
        <Banner username={username} setUsername={setUsername} password={password} setPassword={setPassword} token={token} setToken={setToken} setIsLoggedIn={setIsLoggedIn} getRecommendation={getRecommendation} fees={fees} currentLoans={currentLoans} totalLoans={totalLoans} lateLoans={lateLoans} favoriteGenre={favoriteGenre} bannerDisplay={bannerDisplay} setBannerDisplay={setBannerDisplay} errorMessage={errorMessage} setErrorMessage={setErrorMessage} width={width}/>
      </div>
      <div className='row'>
        <LeftColumn booksOrReviews={booksOrReviews} setBooksOrReviews={setBooksOrReviews} counter={counter} setCounter={setCounter} books={books} setBooks={setBooks} genre={genre} setGenre={setGenre} minValue={minValue} maxValue={maxValue} set_minValue={set_minValue} set_maxValue={set_maxValue} recommendBook={recommendBook} isLoggedIn={isLoggedIn} token={token} refreshMyBooks={refreshMyBooks} setRefreshMyBooks={setRefreshMyBooks} recommendArr={recommendArr} recommendBookIndex={recommendBookIndex} setRecommendBookIndex={setRecommendBookIndex} setRecommendBook={setRecommendBook} reviews={reviews} width={width} select={select} setSelect={setSelect} browseBy={browseBy} setbrowseBy={setbrowseBy}/>
        <MainColumn books={books} setBooks={setBooks} counter={counter} setCounter={setCounter} getAllBooks={getAllBooks} reviews={reviews} genre={genre} minValue={minValue} maxValue={maxValue} token={token} isLoggedIn={isLoggedIn} refreshMyBooks={refreshMyBooks} setRefreshMyBooks={setRefreshMyBooks} filteredText={filteredText} setFilteredText={setFilteredText} booksToShow={booksToShow} setBooksToShow={setBooksToShow} myLoans={myLoans} width={width}/>
        {booksOrReviews === 'reviews' ? <Reviews reviews={reviews} counter={counter} setCounter={setCounter} books={books} setBooksOrReviews={setBooksOrReviews} width={width} /> : null}
        <RightColumn isLoggedIn={isLoggedIn} token={token} books={books} username={username} refreshMyBooks={refreshMyBooks} setRefreshMyBooks={setRefreshMyBooks} myLoans={myLoans} setMyLoans={setMyLoans} readMyReviews={readMyReviews} setReadMyReviews={setReadMyReviews} setFees={setFees} setCurrentLoans={setCurrentLoans} setTotalLoans={setTotalLoans} width={width}/>
      </div>
      <Footer />
    </div>
  )

}

export default App

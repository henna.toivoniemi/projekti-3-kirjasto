import './Banner.css'
import UserInfoWindow from './UserInfoWindow'
import LoginWindow from './LoginWindow'
import RegisterWindow from './RegisterWindow'


//FUNKTIO JOKA LUO YLÄBANNERIN, PROPSIT APP.JSX:STÄ
function Banner({username, setUsername, password, setPassword, token, setToken, setIsLoggedIn, getRecommendation, fees, currentLoans, totalLoans, lateLoans, favoriteGenre, bannerDisplay, setBannerDisplay, errorMessage, setErrorMessage, width}) {
  return (
      <div className='Banner col-4' style={width < 1350 ? {  borderLeft: "5px solid #6A341D"} : {}}>
        {localStorage.getItem('token') !== null
          ? <UserInfoWindow username={username} setUsername={setUsername} password={password} setPassword={setPassword} token={token} setToken={setToken} setIsLoggedIn={setIsLoggedIn} getRecommendation={getRecommendation} fees={fees} currentLoans={currentLoans} totalLoans={totalLoans} lateLoans={lateLoans} favoriteGenre={favoriteGenre} setBannerDisplay={setBannerDisplay} setErrorMessage={setErrorMessage} />
          : bannerDisplay === true
            ? <LoginWindow username={username} setUsername={setUsername} password={password} setPassword={setPassword} setBannerDisplay={setBannerDisplay} setToken={setToken} setIsLoggedIn={setIsLoggedIn} errorMessage={errorMessage} setErrorMessage={setErrorMessage} />
            : <RegisterWindow username={username} setUsername={setUsername} password={password} setPassword={setPassword} setBannerDisplay={setBannerDisplay} setToken={setToken} setIsLoggedIn={setIsLoggedIn} errorMessage={errorMessage} setErrorMessage={setErrorMessage} />
        }
      </div>
  )
}

export default Banner

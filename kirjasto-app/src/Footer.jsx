import { useState, useEffect } from 'react'
import axios from 'axios'
import './Footer.css'

function Footer() {

  const [quote, setQuote] = useState([])

  const arrayOfQuotes = [
    {content: "And now that you don't have to be perfect, you can be good.", author: "John Steinbeck"},
    {content: "I haven't had a very good day. I think I might still be hungover and everyone's dead and my root beer's gone.", author:"Holly Black"},
    {content:"But I am very poorly today & very stupid & I hate everybody & everything. One lives only to make blunders.", author: "Charles Darwin"},
    {content: "Monsters are real, ghosts are real, too. They live inside us, and sometimes they win.", author: "Stephen King"},
    {content: "I want to taste and glory in each day, and never be afraid to experience pain.", author: "Sylvia Plath"},
    {content: "As you start to walk on the way, the way appears.", author: "Rumi"},
    {content: "Dwell in possibility.", author: "Emily Dickinson"},
    {content: "When you can't change the direction of the wind, adjust your sails.", author: "H. Jackson Brown Jr."},
    {content: "Laugh loudly, laugh often, and most important, laugh at yourself.", author: "Chelsea Handler"},
    {content: "Nothing can dim the light that shines from within.", author: "Maya Angelou"},
    {content: "The most common way people give up their power is by thinking they don't have any.", author: "Alice Walker"},
    {content: "Tomorrow is always fresh, with no mistakes in it yet.", author: "L. M. Montgomery"},
    {content: "I'm not afraid of storms, for I'm learning how to sail my ship.", author: "Louisa May Alcott"},
    {content: "Don't cry because it's over. Smile because it happened.", author: "Dr. Seuss"},
    {content: "Anyone who has never made a mistake has never tried anything new.", author: "Albert Einstein"},
    {content: "Everything is hard before it is easy.", author: "Johann Wolfgang von Goethe"},
    {content: "We are all broken, that's how the light gets in.", author: "Ernest Hemingway"},
    {content: "Some of us think holding on makes us strong; but sometimes it is letting go.", author: "Herman Hesse"},
    {content: "Never look back unless you are planning to go that way.", author: "Henry David Thoreau"},
    {content: "We rise by lifting others.", author: "Robert Ingersoll"}
  ]

  useEffect(() => {
    const getQuote = () => {
      const randomIndex = Math.floor(Math.random()*20)
      const selectedQuote = arrayOfQuotes[randomIndex]
      setQuote(selectedQuote)
    }
    getQuote()
  }, [])

  return (
    <div className='Footer'>
      <p className='quote'>{quote.content}</p>
      <p className='author'>{`- ${quote.author}`}</p>
    </div>
  )
}

export default Footer

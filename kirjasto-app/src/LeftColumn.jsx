import MultiRangeSlider from "multi-range-slider-react"
import './Multirangeslider.css'
import './LeftColumn.css'
import Recommendations from "./Recommendations"

function LeftColumn(props) {
    
    //Funktio joka asettaa arvon vuosirangelle
    const handleInput = (e) => {
        props.set_minValue(e.minValue)
        props.set_maxValue(e.maxValue)
        props.setCounter(0)
    }

    //Funktio jossa sortataan books lista uusiksi
    function onselectChange(event){
        if(event.target.value === 'byAuthor'){
            props.setBooks(([...props.books].sort(
                (a, b) => 
                    (a.author.split(' ')[1] > b.author.split(' ')[1]) ? 1 : (a.author.split(' ')[1] < b.author.split(' ')[1]) ? -1 : 0)  ))
            props.setSelect('byAuthor')
        }
        if(event.target.value === 'highestRating'){
            props.setBooks(([...props.books].sort(
                (a, b) => 
                (a.average < b.average) ? 1 : (a.average > b.average) ? -1 : 0) ))
            props.setSelect('highestRating')
        }
        if(event.target.value === 'lowestRating'){
            props.setBooks(([...props.books].sort(
                (a, b) => 
                (a.average > b.average) ? 1 : (a.average < b.average) ? -1 : 0) ))
            props.setSelect('lowestRating')
        }
        props.setCounter(0)
    }
 
    //Funktio joka asettaa genren 
    function onGenreChange(event){
        if(event.target.value === 'All'){
            props.setGenre('All')
        }
        if(event.target.value === 'Children'){
            props.setGenre('Children')
        }
        if(event.target.value === 'Fantasy'){
            props.setGenre('Fantasy')
        }
        if(event.target.value === 'History'){
            props.setGenre('History')
        }
        if(event.target.value === 'Horror'){
            props.setGenre('Horror')
        }
        if(event.target.value === 'Mystery'){
            props.setGenre('Mystery')
        }
        if(event.target.value === 'Romance'){
            props.setGenre('Romance')
        }
        if(event.target.value === 'Sci-fi'){
            props.setGenre('Sci-fi')
        }
        if(event.target.value === 'Self-help'){
            props.setGenre('Self-help')
        }
        if(event.target.value === 'Self-help'){
            props.setGenre('Self-help')
        }
        if(event.target.value === 'Philosophy'){
            props.setGenre('Philosophy')
        }
        if(event.target.value === 'Western'){
            props.setGenre('Western')
        }
        props.setCounter(0)
    }

    //Funtkio joka palauttaa oletusfiltterit
    function chaingeFilters(){
        props.setbrowseBy(!props.browseBy)
        props.setCounter(0)
        props.setGenre('All')
        props.set_minValue(1900)
        props.set_maxValue(2024)
        props.setBooks(([...props.books].sort(
            (a, b) => 
                (a.author.split(' ')[1] > b.author.split(' ')[1]) ? 1 : (a.author.split(' ')[1] < b.author.split(' ')[1]) ? -1 : 0)  ))
        props.setSelect('byAythor')
    }
 
    return (
        <div className="col-2 col-s-4 LeftColumn" style={props.width < 700 ? { height: "fit-content"} : {}}>
        
            <div className='leftRadioButtons'>
            Books
            <input type="radio" id="allBooks" name="booksOrReviews" value="books" checked={props.booksOrReviews === 'books'}
                onChange={() => {
                    props.setBooksOrReviews('books')
                    props.setCounter(0)
                }
                }>
            </input>
           
            Reviews
            <input type="radio" id="allReviews" name="booksOrReviews" value="reviews" checked={props.booksOrReviews === 'reviews'}
                onChange={() => {
                    props.setBooksOrReviews('reviews')
                    props.setCounter(0)
                }}>
            </input>
            </div>

            <div>
            <label className="searchforBooks">Browse by filter
            <input type="checkbox" id="searchforBooks" name="searchforbooks" value={props.browseBy} 
            onChange={() => chaingeFilters()}/>
            </label>
               {props.browseBy === true 
               ?
               <fieldset >
               <label>Genre: 
               <select name="genre" id="genre"  onChange={onGenreChange} value={props.genre}>
                   <option value="All">All</option>
                   <option value="Children">Children</option>
                   <option value="Fantasy">Fantasy</option>
                   <option value="History">History</option>
                   <option value="Horror">Horror</option>
                   <option value="Mystery">Mystery</option>
                   <option value="Romance">Romance</option>
                   <option value="Sci-fi">Sci-fi</option>
                   <option value="Self-help">Self-help</option>
                   <option value="Philosophy">Philosophy</option>
                   <option value="Western">Western</option>
               </select> 
               </label>

               <label>Sort: 
               <select name="rating" id="rating" onChange={onselectChange} value={props.select}>
                   <option value="byAuthor">By Author</option>
                   <option value="highestRating">Best rated</option>
                   <option value="lowestRating">Lowest rated</option>
               </select> 
               </label>
               <label>
                   Year:
                   <MultiRangeSlider
                       min={1900}
                       max={2024}
                       step={10}
                       minValue={props.minValue}
                       maxValue={props.maxValue}
                       onChange={(e) => {
                           handleInput(e);
                       }}
                   />
               </label>
               </fieldset>
               :
               <Recommendations recommendBook={props.recommendBook} isLoggedIn={props.isLoggedIn} books={props.books} token={props.token} refreshMyBooks={props.refreshMyBooks} setRefreshMyBooks={props.setRefreshMyBooks} recommendArr={props.recommendArr} recommendBookIndex={props.recommendBookIndex} setRecommendBookIndex={props.setRecommendBookIndex}  setRecommendBook={props.setRecommendBook} reviews={props.reviews} width={props.width}/>
                }
            </div>
        </div>)
}

export default LeftColumn
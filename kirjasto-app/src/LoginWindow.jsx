import { useState } from 'react'
import './LoginWindow.css'
import axios from 'axios'



const LoginWindow = ({ username, setUsername, password, setPassword, setBannerDisplay, setToken, setIsLoggedIn, errorMessage, setErrorMessage }) => {

    //SISÄÄNKIRJAUTUMINEN BÄKKÄRILLE
    const login = async (username, password) => {
        const url = 'https://buutti-library-api.azurewebsites.net/api/users/login'
        const body = { 'username': username, 'password': password }
        try {
            const response = await axios.post(url, body)
            setToken(response.data.token)
            setIsLoggedIn(true);
            localStorage.setItem('token', response.data.token)
            localStorage.setItem('username', response.data.username)
            console.log(token);
        } catch (error) {
            setErrorMessage('Invalid username or password')
            return { errorMessage }
        }
    }

    const handleKeyDown = (event) => {
        if (event.key === 'Enter') {
            login(username, password)
        }
    }

    return (
        <div className='loginContainer'>
            <div className='loginHeader'><h2>Login</h2></div>
            <div className='loginWindow'>
                <label className='loginUsername'>
                    <p>Username:</p>
                    <input type='text' value={username} onChange={(event) => {
                        setUsername(event.target.value)
                        setErrorMessage('')
                    }}
                        onKeyDown={handleKeyDown} />
                </label>
                <label className='loginPassword'>
                    <p>Password:</p>
                    <input type='password' value={password} onChange={(event) => {
                        setPassword(event.target.value)
                        setErrorMessage('')
                    }}
                        onKeyDown={handleKeyDown} />
                </label>
                <div className='buttons'>
                    <button className='loginBtn' onClick={() => login(username, password)}>Login</button>
                    <span className='loginOrCreateAccount' onClick={() => {
                        setBannerDisplay(false)
                        setErrorMessage('')
                    }}>Create account</span>
                </div>
                <p className='loginError'>{errorMessage}</p>
            </div>
        </div>
    )
}



export default LoginWindow
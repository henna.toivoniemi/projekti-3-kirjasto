import './MainColumn.css'
import axios from 'axios'
import Search from './Search'
import { useState, useRef, useEffect } from 'react'
import { SlArrowLeft } from "react-icons/sl"
import { SlArrowRight } from "react-icons/sl"
import ShowBookReview from './ShowBookReview'
import TotalBooksToShow from './TotalBooksToShow'
import { GiSpiderWeb, GiBalloonDog, GiSpikedDragonHead, GiScrollQuill, GiShatteredGlass, GiHeartKey, GiRingedPlanet, GiThink, GiHummingbird, GiRevolver } from "react-icons/gi"
import getStars from './getStars'
import React from 'react'
import { useSound } from 'use-sound'
import success from './assets/short-success.mp3'
import failure from './assets/failure.mp3'

function MainColumn(props) {

  //Tilamuuttuja jossa tallessa avatun kirja-arvostelun isbn
  const [showBookReviewIsbn, setBookReview] = useState('')
  const [loanMessage, setLoanMessage] = useState('Thanks for loaning!')
  const [clickedBook, setClicked] = useState('')
  const [numberOfBooks, setNumberOfBooks] = useState(5)
  const [playSuccess] = useSound(success)
  const [playFailure] = useSound(failure)

 //Näytetään kirjoja näytön leveyden mukaan. UseEffect seuraa näytön leveyttä
 useEffect(() => {
  if(props.width >= 1870){
    setNumberOfBooks(7)
  }
  if(props.width < 1870){
    setNumberOfBooks(6)
  }
  if(props.width < 1620){
    setNumberOfBooks(5)
  }
  if(props.width < 1415){
    setNumberOfBooks(4)
  }
  if(props.width < 1230){
    setNumberOfBooks(3)
  }
  // if(props.width < 1100){
  //   setNumberOfBooks(2)
  // }
}, [props.width])

  //Funktio joka hakee ikonit genren mukaan
  function getIcons(genre) {
    if (genre === "Children") {
      return <GiBalloonDog className='iconImg' />
    }
    if (genre === "Horror") {
      return <GiSpiderWeb className='iconImg' />
    }
    if (genre === "Fantasy") {
      return <GiSpikedDragonHead className='iconImg' />
    }
    if (genre === "History") {
      return <GiScrollQuill className='iconImg' />
    }
    if (genre === "Mystery") {
      return <GiShatteredGlass className='iconImg' />
    }
    if (genre === "Romance") {
      return <GiHeartKey className='iconImg' />
    }
    if (genre === "Sci-fi") {
      return <GiRingedPlanet className='iconImg' />
    }
    if (genre === "Philosophy") {
      return <GiThink className='iconImg' />
    }
    if (genre === "Self-help") {
      return <GiHummingbird className='iconImg' />
    }
    if (genre === "Western") {
      return <GiRevolver className='iconImg' />
    }
  }

  //Funktio kirjan lainaamiselle
  async function loanBook(isbn, genre) {
    // console.log(ref.current)
    // ref.current.className = `bookInfo loaned-book ${genre}`
    setClicked(isbn)
    const url = 'https://buutti-library-api.azurewebsites.net/api/loans/' + isbn
    const config = {
      headers: { "Authorization": "Bearer " + props.token }
    }
    try {
      const response = await axios.post(url, {}, config)
      props.setRefreshMyBooks(!props.refreshMyBooks)
      setLoanMessage('Thanks for loaning!')
      playSuccess()
    } catch (error) {
      setLoanMessage('This book is currently not available')
      playFailure()
    }
  }

    //Funktio joka tarkastaa onko kirja käyttäjällä lainassa
    function checkMyLoans(isbn){
      let loaned = false
      props.myLoans.find(item => {
      if(item.book_isbn === isbn && item.returned === null){
        loaned = true
      }
      }
      )
    return loaned
    }

     // Funktio joka laskee kirjat asetetuilla filtereillä
     function countBooksToShow(){
      const filteredBooks = props.books
      .filter(book => props.genre === 'All' ? true : book.genre === props.genre)
      .filter(book => book.year >= props.minValue && book.year <= props.maxValue) 
      .filter(book => book.title.toLowerCase().includes(props.filteredText.toLowerCase()) || book.author.toLowerCase().includes(props.filteredText.toLowerCase())) 
      props.setBooksToShow(filteredBooks.length)
      return(filteredBooks.length)
    }

  return (
    <div className='col-7 col-s-8 MainColumn'>
      <Search filteredText={props.filteredText} setFilteredText={props.setFilteredText} counter={props.counter} setCounter={props.setCounter}/>
      <div className='navigation'>
      <NavigationStart counter={props.counter} setCounter={props.setCounter} numberOfBooks={numberOfBooks}/>
      <TotalBooksToShow counter={props.counter} genre={props.genre} books={props.books} minValue={props.minValue} maxValue={props.maxValue} filteredText={props.filteredText} booksToShow={props.booksToShow} setBooksToShow={props.setBooksToShow} numberOfBooks={numberOfBooks} setNumberOfBooks={setNumberOfBooks} countBooksToShow={countBooksToShow}/>
      <NavigationEnd counter={props.counter} setCounter={props.setCounter} books={props.books} genre={props.genre} booksToShow={props.booksToShow} numberOfBooks={numberOfBooks}/>
      </div>
      <div className='MainColumnContainer'>
        {props.books
          .filter(book => props.genre === 'All' ? true : book.genre === props.genre)
          .filter(book => book.year >= props.minValue && book.year <= props.maxValue)
          .filter(book => book.title.toLowerCase().includes(props.filteredText.toLowerCase()) || book.author.toLowerCase().includes(props.filteredText.toLowerCase()))
          .map((book, i) => {
            if (i < (props.counter + numberOfBooks) && i >= props.counter) {
              return (
                <div className={book.isbn !== clickedBook ? `bookInfo ${book.genre}` : `bookInfo ${book.genre} loaned-book`} key={book.isbn}>
                  <div className='side spine'>
                    <p className='bookAuthor'>{book.author}</p>
                    <div className='bookCenterColumn'>
                      <p>{book.title}</p>
                      <p>{book.year}</p>
                      <p>{getStars(book.average)}</p>
                      <div>{book.average > 0 ? <button className='loanButton' onClick={() => setBookReview(book.isbn)}>Reviews</button> : null}</div>
                      <div>{props.isLoggedIn === true ? <button className='loanButton' onClick={() => loanBook(book.isbn, book.genre)} disabled={checkMyLoans(book.isbn)}>Loan</button> : null}</div>
                    </div>
                    <p className='icons'>{getIcons(book.genre)}</p>
                  </div>
                  <div className="side top"></div>
                  <div className={`side cover ${book.genre}`} onClick={() => setClicked('')}>
                    <p style={{fontSize: "14pt"}}>{loanMessage}</p>
                    <p style={{fontSize: "14pt"}}>Click to close</p>
                  </div>
                </div>
              )
            }

          }
          )}

        {showBookReviewIsbn !== ''
          //Jos painettu show review avataan arvostelut. 
          ?
          <ShowBookReview showBookReviewIsbn={showBookReviewIsbn} setBookReview={setBookReview} books={props.books} reviews={props.reviews} />
          :
          null
        }
      </div>
      {countBooksToShow() === 0 ? <div className='post-it'>No results</div> : null}
      <div className='shelf'></div>
    </div>
  )
}

export default MainColumn


//Komponentti joka näyttää nuolen vasemmalle tarvittaessa
function NavigationStart(props) {

  return (
    props.counter >= props.numberOfBooks
      ?
      <button className='navigationBtn' onClick={() => props.setCounter(props.counter - props.numberOfBooks)}><SlArrowLeft /></button>
      :
      <button className='navigationBtn' disabled={true}><SlArrowLeft /></button>
  )
}

//Komponentti joka näyttää nuolen oikealle tarvittaessa
function NavigationEnd(props) {

  return (
    props.counter + props.numberOfBooks < props.booksToShow
      ?
      <button className='navigationBtn' onClick={() => props.setCounter(props.counter + props.numberOfBooks)}><SlArrowRight /></button>
      :
      <button className='navigationBtn' disabled={true}><SlArrowRight /></button>
  )
}

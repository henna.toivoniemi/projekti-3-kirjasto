import './Reviews.css'
import { useEffect } from 'react'
import axios from 'axios'
import { IoClose } from "react-icons/io5"
import { MdDeleteForever } from "react-icons/md"
import getStars from './getStars'


//Komponentti joka renderöi divin/"ikkunan", missä kaikki omat arvostelut. Jokaisella arvostelulla on poista-nappi ja ikkunan voi myös sulkea.
function MyReviews(props) {
    const { readMyReviews, setReadMyReviews, myReviews, setMyReviews, username, token, getBookName } = props

    useEffect(() => {
        getMyReviews()
    }, [setReadMyReviews, readMyReviews])

    // funktio joka hakee omat arvostelut ja tallettaa ne tilamuuttujaan
    const getMyReviews = async () => {
        const url = 'https://buutti-library-api.azurewebsites.net/api/reviews/'
        const response = await axios.get(url)
        const allReviews = response.data.reverse()
        const myNewReviews = allReviews.filter(item => item.username === username)
        setMyReviews(myNewReviews)
    }

    //funktio joka poistaa oman arvostelun
    const deleteMyReview = async (id) => {
        const url = 'https://buutti-library-api.azurewebsites.net/api/reviews/' + id
        const config = {
            headers: { "Authorization": "Bearer " + token }
        }
        const response = await axios.delete(url, config)
        getMyReviews()
    }

    return (
        <>
            <div className='grey' onClick={() => setReadMyReviews(false)}></div>
            <div className='MyContainer'  style={props.width < 700 ? { width: "95%"} : {}}>
                <div className='MyHeader'>
                    <h2>My reviews (total: {myReviews.length})</h2>
                    <p className='closeButton' onClick={() => props.setReadMyReviews(false)}><IoClose /></p>
                </div>
                <div className='ReviewContainer'>
                    <div className='Reviews'>
                        {myReviews.map(item => {
                            return <div className='mySingleReview' key={item.id}>
                                <div className='bookHeader'>
                                    <p className='deleteButton' onClick={() => deleteMyReview(item.id)}><MdDeleteForever /></p>
                                    <p className='bookTitle'>{getBookName(item.book_isbn)}</p>
                                </div>
                                <p className='bookRating'>{getStars(item.rating)}</p>
                                <p className='bookReview'>{item.review}</p>
                                <p className='filler'></p>
                            </div>
                        })
                        }
                    </div>
                </div>
            </div>
        </>

    )

}



export default MyReviews
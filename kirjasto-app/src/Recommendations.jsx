import { useEffect, useState } from 'react'
import axios from 'axios';
import './Recommendations.css'
import { GiSpiderWeb, GiBalloonDog, GiSpikedDragonHead, GiScrollQuill, GiShatteredGlass, GiHeartKey, GiRingedPlanet, GiThink, GiHummingbird, GiRevolver } from "react-icons/gi"
import { IoMdStar } from "react-icons/io";
import { IoMdStarHalf } from "react-icons/io";
import ShowBookReview from './ShowBookReview';
import { useSound } from 'use-sound'
import success from './assets/short-success.mp3'
import failure from './assets/failure.mp3'
import { IoClose } from "react-icons/io5";
import getStars from './getStars';

function Recommendations(props) {

  //Tilamuuttuja jos halutaan tarkastella kirjan arvosteluja
  const [showBookReviewIsbn, setBookReview] = useState('')
  const [playSuccess] = useSound(success)
  const [playFailure] = useSound(failure)
  //Tilamuuttuja boolean arvolla jos kirja ei ole vapaana muuttuu true
  const [bookNotAvailable, setBookNotAvailable] = useState(false)

  //Funktio joka hakee ikonit genren mukaan
  function getIcons(genre) {
    if (genre === "Children") {
      return <GiBalloonDog className='iconImg' />
    }
    if (genre === "Horror") {
      return <GiSpiderWeb className='iconImg' />
    }
    if (genre === "Fantasy") {
      return <GiSpikedDragonHead className='iconImg' />
    }
    if (genre === "History") {
      return <GiScrollQuill className='iconImg' />
    }
    if (genre === "Mystery") {
      return <GiShatteredGlass className='iconImg' />
    }
    if (genre === "Romance") {
      return <GiHeartKey className='iconImg' />
    }
    if (genre === "Sci-fi") {
      return <GiRingedPlanet className='iconImg' />
    }
    if (genre === "Philosophy") {
      return <GiThink className='iconImg' />
    }
    if (genre === "Self-help") {
      return <GiHummingbird className='iconImg' />
    }
    if (genre === "Western") {
      return <GiRevolver className='iconImg' />
    }
  }


  //Funktio kirjan lainaamiselle
  async function loanBook(isbn) {
    console.log(isbn)
    console.log(props.token)
    const url = 'https://buutti-library-api.azurewebsites.net/api/loans/' + isbn
    const config = {
      headers: { "Authorization": "Bearer " + props.token }
    }
    try {
      const response = await axios.post(url, {}, config)
      props.setRefreshMyBooks(!props.refreshMyBooks)
      props.recommendArr.splice[props.recommendBookIndex,1]
      playSuccess()
    } catch (error) {
      playFailure()
      setBookNotAvailable(true)
    }
  }

  //Funktio joka hakee suosittelulistalta seuraavan kirjan
  function chaingeRecom(){
      if(props.recommendBookIndex < props.recommendArr.length -1 ){
        props.setRecommendBookIndex(props.recommendBookIndex + 1)
    }
    else{
      props.setRecommendBookIndex(0)
    }
    props.setRecommendBook(props.recommendArr[props.recommendBookIndex])
  }

  return(
    <div>
        {props.isLoggedIn === true && props.recommendBook !== undefined 
        ?
        <div className='recomContainer'>
        <p className='recomHeader'>Recommendations for you</p>
        <div className='recomBook' >
          <p className='recomAuthor'>{props.recommendBook.author}</p>
          <p className='recomTitle'>{props.recommendBook.title}</p>
          <p className='recomYear'>{props.recommendBook.year}</p>
          <p className='recomStars'>{getStars(props.recommendBook.average)}</p>
          <div>{props.recommendBook.average > 0 ? <button className='loanButton' onClick={() => setBookReview(props.recommendBook.isbn)}>Reviews</button> : null}</div>
          <p><button className='loanButton' onClick={() => loanBook(props.recommendBook.isbn)}>Loan</button></p>
          <p className='recomGenre'>{getIcons(props.recommendBook.genre)}</p>
        </div>
        <button className='changeRecommendationBtn' onClick={() => chaingeRecom()}>Recommend another</button>
      </div>
        :
        <></>
      }
     {showBookReviewIsbn !== ''
          //Jos painettu show review avataan arvostelut. 
          ?
          <ShowBookReview showBookReviewIsbn={showBookReviewIsbn} setBookReview={setBookReview} books={props.books} reviews={props.reviews} />
          :
          null
      }
       {bookNotAvailable === true
          ?
          <>
          <div className='grey' onClick={() => setBookNotAvailable(false)}>
          
          </div>
         
          <div className='notAvailableMsg'>
          
          <div className='notAvailableMsgTitle'>
          <p className='closeButton' onClick={() => setBookNotAvailable(false)}><IoClose /></p>
            {props.recommendBook.title}
           
          </div>
          <div className='bookNotAvailableMsg'>Book not available. Try again later</div>
          </div>
          
          </>
          :
          null
        }
  </div>  
)
}

export default Recommendations

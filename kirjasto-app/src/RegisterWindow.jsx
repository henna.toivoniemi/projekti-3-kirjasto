import { useState } from 'react'
import axios from 'axios'
import './RegisterWindow.css'

//REKISTERÖITYMISIKKUNA-KOMPONENTTI
const RegisterWindow = ({ username, setUsername, password, setPassword, setBannerDisplay, setToken, setIsLoggedIn, errorMessage, setErrorMessage }) => {
    //tilamuuttuja johon tallennetaan käyttäjän uudelleen syöttämä salasana (salasanan vahvistus)
    const [password2, setPassword2] = useState('')
    //tilamuuttuja joka määrittää sen voiko Create account -nappia painaa (true=ei voi painaa)
    const [disabled, setDisabled] = useState(true)

    //REKISTERÖITYMINEN BÄKKÄRILLE
    const register = async (username, password) => {
        try {
            const url = 'https://buutti-library-api.azurewebsites.net/api/users/register'
            const body = { username, password }
            // console.log(username, password)
            const response = await axios.post(url, body)
            setToken(response.data.token)
            localStorage.setItem('token', response.data.token)
            localStorage.setItem('username', response.data.username)
            setIsLoggedIn(true)
        } catch (error) {
            setErrorMessage("Username is already taken")
            return { errorMessage }
        }

    }

    //napin klikkaaminen toimii myös enterillä
    const handleKeyDown = (event) => {
        if (event.key === 'Enter' && disabled === false) {
            register(username, password)
        }
    }

    const checkPassword1 = (password) => {
        setPassword(password)
        if (password.length < 4) {
            setErrorMessage('Password is too short!')
        } else {
            setErrorMessage('')
        }
    }

    const verifyPassword2 = (password2) => {
        setPassword2(password2)
        //jos salasanat vastaa toisiaan JA input fieldit EI ole tyhjiä 
        if (password === password2 && password2 !== '' && username !== '') {
            setDisabled(false)
            setErrorMessage('Passwords match')
            //jos salasanat EI vastaa toisiaan JA input fieldit EI ole tyhjiä
        } else if (password !== password2 && password2 !== '') {
            setDisabled(true)
            setErrorMessage('Passwords do not match')
        }
    }

    return (
        <div className='registerContainer'>
            <div className='registerHeader'><h2>Create Account</h2></div>
            <div className='registerWindow'>
                <label className='regUsername'>
                    <p>Username:</p>
                    <input type='text' value={username} onChange={(event) => setUsername(event.target.value)} />
                </label>
                <label className='regPassword'>
                    <p>Password:</p>
                    <input type='password' value={password} onChange={(event) => checkPassword1(event.target.value)} />
                </label>
                <label className='regConfirmPassword'>
                    <p>Confirm password:</p>
                    <input type='password' value={password2} onChange={(event) => verifyPassword2(event.target.value)} onKeyDown={handleKeyDown} />
                </label>
                <p className='regError'>{errorMessage}</p>
                <div className='regButtons'>
                    <button className='loginBtn' onClick={() => {
                        register(username, password)
                        setPassword2('')
                    }} disabled={disabled}>Create account</button>
                    <span className='loginOrCreateAccount' onClick={() => {
                        setBannerDisplay(true)
                        setErrorMessage('')
                    }}>Login</span>
                </div>
            </div>
        </div>
    )
}

export default RegisterWindow
import './ReviewWindow.css'
import { useState } from 'react'
import axios from 'axios'
import { IoMdStar } from "react-icons/io"
import { IoMdStarOutline } from "react-icons/io"
import { IoClose } from "react-icons/io5"



function ReviewWindow({ setWriteReview, isbn, title, token, refreshMyBooks, setRefreshMyBooks }) {
    const [review, setReview] = useState('')
    const [rating, setRating] = useState(0)
    const [disabled, setDisabled] = useState(true)
    const [clickCheck, setClickCheck] = useState(true)

    //SUBMIT-NAPIN FUNKTIO
    async function submit() {
        const url = 'https://buutti-library-api.azurewebsites.net/api/reviews/' + isbn
        const config = {
            headers: { "Authorization": "Bearer " + token }
        }

        //body määräytyy sen mukaan onko annettu vain review/rating vai molemmat
        let body = ''
        if (rating === 0 && review !== '') {
            body = { "review": review, "rating": undefined }
        } else if (rating !== 0 && review === '') {
            body = { "review": undefined, "rating": rating }
        } else if (rating !== 0 && review !== '') {
            body = { "review": review, "rating": rating }
        }

        const response = await axios.post(url, body, config)
        //submitin jälkeen nollataan kaikki
        setReview('')
        setRating('')
        setDisabled(true)
        setWriteReview(false)
        setRefreshMyBooks(!refreshMyBooks)
    }

    //CANCEL-NAPIN FUNKTIO = nollataan kaikki
    function cancel() {
        setReview('')
        setRating('')
        setDisabled(true)
        setWriteReview(false)
    }

    //TÄHTIEN LISÄÄMINEN (tällä hetkellä puolikkaita tähtiä ei voi antaa)
    function checkStars(number) {
        //submit-nappi ei ole enää disabled kun pisteitä annettu
        setDisabled(false)
        //rating-tilamuuttujaan numero joka tulee parametrina klikatusta tähdestä riippuen
        setRating(number)
        //tästä eteenpäin en enää ymmärrä miksi tämä toimii, joten en osaa kommentoida (toimii oikein joka tapauksessa!)
        let clicks = number - 1
        setClickCheck(!clickCheck)
        clickCheck === true ? clicks++ : null
        setRating(clicks)
    }

    return (
        <>
            <div className='grey' onClick={() => setWriteReview(false)}></div>
            <div className='ReviewWindow'>
                <div className='header'>
                    <h2>Review for <i>{title}</i></h2>
                    <p className='closeButton' onClick={cancel}><IoClose /></p>
                </div>
                <textarea autoFocus={true} placeholder='Start writing' maxLength='200' rows='4' value={review} onChange={(event) => {
                    setReview(event.target.value)
                }}></textarea>
                <div className='StarRating'>
                    <p>Give a rating: </p>
                    <div className='stars'>
                        <div onClick={() => checkStars(1)}>{(rating >= 1) ? <IoMdStar /> : <IoMdStarOutline />}</div>
                        <div onClick={() => checkStars(2)}>{(rating >= 2) ? <IoMdStar /> : <IoMdStarOutline />}</div>
                        <div onClick={() => checkStars(3)}>{(rating >= 3) ? <IoMdStar /> : <IoMdStarOutline />}</div>
                        <div onClick={() => checkStars(4)}>{(rating >= 4) ? <IoMdStar /> : <IoMdStarOutline />}</div>
                        <div onClick={() => checkStars(5)}>{(rating >= 5) ? <IoMdStar /> : <IoMdStarOutline />}</div>
                    </div>
                    <div className='StarsAsNumber'>{rating > 0 ? rating : null}</div>
                </div>
                <div className='filler'></div>
                <div className='ReviewButtons'>
                    <button className='submit' onClick={submit} disabled={disabled}>Submit</button>
                </div>
                <SubmitButtonControl review={review} rating={rating} setDisabled={setDisabled} />
            </div>
        </>
    )

}

//FUNKTIO JOKA TARKISTAA VOIKO SUBMITIA PAINAA (sekä review/rating ei saa olla tyhjiä)
const SubmitButtonControl = ({ review, rating, setDisabled }) => {
    (review === '' && rating === 0) ? setDisabled(true) : setDisabled(false)
}


export default ReviewWindow
import './Reviews.css'
import { IoMdStar } from "react-icons/io"
import { IoMdStarHalf } from "react-icons/io"
import { IoClose } from "react-icons/io5";
import { useState } from 'react'
import getStars from './getStars';
import { IoMdStarOutline } from "react-icons/io";


function Reviews(props) {

  const [searchText, setSearchText] = useState('')
  const [rating, setRating] = useState(0)
  const [clickCheck, setClickCheck] = useState(true)

  const initialSearchText = 'Search review by username...'

  //Funktio joka hakee kirjan nimen
  function getBookName(isbn) {
    for (let i = 0; i < props.books.length; i++) {
      if (props.books[i].isbn === isbn) {
        return props.books[i].title
      }
    }
  }

  //TÄHTIEN LISÄÄMINEN (tällä hetkellä puolikkaita tähtiä ei voi antaa)
  function checkStars(number) {
    //rating-tilamuuttujaan numero joka tulee parametrina klikatusta tähdestä riippuen
    setRating(number)
    //tästä eteenpäin en enää ymmärrä miksi tämä toimii, joten en osaa kommentoida (toimii oikein joka tapauksessa!)
    let clicks = number - 1
    setClickCheck(!clickCheck)
    clickCheck === true ? clicks++ : null
    setRating(clicks)
  }

  return (
    <>
      <div className='grey' onClick={() => props.setBooksOrReviews('books')}></div>
      <div className='ReviewsContainer' style={props.width < 700 ? { width: "95%"} : {}}>
        <div className='ReviewsHeader'>
          <h2>All reviews (total: {props.reviews.length})</h2>
          <input type='text'
            placeholder={initialSearchText}
            className='searchByUser'
            value={searchText}
            onChange={(event) =>
              setSearchText(event.target.value)
            }
          ></input>
          <div className='searchByStars'>
            <p>Min rating: </p>
            <div className='AllReviewsStars'>
              <div onClick={() => checkStars(1)}>{(rating >= 1) ? <IoMdStar /> : <IoMdStarOutline />}</div>
              <div onClick={() => checkStars(2)}>{(rating >= 2) ? <IoMdStar /> : <IoMdStarOutline />}</div>
              <div onClick={() => checkStars(3)}>{(rating >= 3) ? <IoMdStar /> : <IoMdStarOutline />}</div>
              <div onClick={() => checkStars(4)}>{(rating >= 4) ? <IoMdStar /> : <IoMdStarOutline />}</div>
              <div onClick={() => checkStars(5)}>{(rating >= 5) ? <IoMdStar /> : <IoMdStarOutline />}</div>
            </div>
            {/* <div className='number'>{rating > 0 ? rating : null}</div> */}
          </div>
          <p className='closeButton' onClick={() => props.setBooksOrReviews('books')}><IoClose /></p>
        </div>
        <div className='ReviewContainer'>
          <div className='Reviews'>
            {props.reviews.map((review) => {
              return (
                review.username.toLowerCase().includes(searchText.toLowerCase()) && review.rating >= rating
                  ?
                  <div className='singleReview' key={review.id}>
                    <div className='bookHeader'>
                      <p className='reviewID'>id: {review.id}</p>
                      <p className='bookTitle'>{getBookName(review.book_isbn)}</p>
                    </div>
                    <p className='bookRating'>{getStars(review.rating)}</p>
                    <p className='bookReview'>{review.review}</p>
                    <p className='bookReviewer'>{review.username}</p>
                    <p className='filler'></p>
                  </div>
                  :
                  <></>
              )
            })}
          </div>
        </div>
      </div>
    </>
  )
}

export default Reviews

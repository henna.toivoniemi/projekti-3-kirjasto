import './RightColumn.css'
import axios from 'axios'
import { useEffect, useState } from 'react'
import ReviewWindow from './ReviewWindow'
import MyReviews from './MyReviews'
import coffeeAndBooks from './assets/books-coffee.png'


// Komponentti jolla näkyviin käyttäjän tämänhetkiset lainat
function MyLoansComponent(props) {

  const { myLoans, token, setMyLoans, getBookName, getDueDate, books, extendLoan, returnLoan, refreshMyBooks, setRefreshMyBooks, showReviewWindow, setCurrentLoans, setTotalLoans } = props

  // useEffect seuraa jos nappeja (pidennä lainaa tai palauta) painetaan -> hakee uudestaan omat lainat ja päivittää näkymää
  useEffect(() => {
    // console.log('token', token)
    getMyLoans()
  }, [setRefreshMyBooks, refreshMyBooks])

  const getMyLoans = async () => {
    let currentLoansCounter = 0
    const url = 'https://buutti-library-api.azurewebsites.net/api/loans/'
    const config = {
      headers: { "Authorization": "Bearer " + token }
    }
    const response = await axios.get(url, config)
    setMyLoans(response.data.reverse())
    setTotalLoans(response.data.length)
    response.data.map(item => {
      if (item.returned === null) {
        currentLoansCounter++
      }
    })
    setCurrentLoans(currentLoansCounter)
  }

  // tarkistetaan onko laina myöhässä ja jos on, vaihdetaan eräpäivän väri punaiseksi 
  const getStyleForDate = (date) => {
    const lateStyle = { color: 'red', fontWeight: 'bold' }
    const today = new Date().getTime()
    if (today > date) {
      return lateStyle
    }
  }

  //palauttaa divin jossa omat lainat (kirjan nimi + lainauspäivä + eräpäivä)
  return (
    <>
      <h2 className='MyLoanHeader'>My loans:</h2>
      <div className='MyLoans'>
        {myLoans.map(item => {
          if (item.returned === null) {
            return <p key={item.book_isbn}>
              <p className='loan'>{getBookName(item.book_isbn, books)}</p>
              <p className='date' style={getStyleForDate(item.due)}>{`Loan due: ${getDueDate(item.due)}`}</p>
              <button className='rightColumnBtn' id={item.book_isbn} onClick={() => returnLoan(item.book_isbn)}>Return</button>
              <button className='rightColumnBtn' id={item.book_isbn} onClick={() => extendLoan(item.book_isbn)}>Extend loan</button>
              <button className='reviewButton rightColumnBtn' onClick={() => showReviewWindow(item.book_isbn)}>Write a review</button>
            </p>
          } else {
            return null
          }

        })}
      </div>
    </>
  )
}

// Komponentti jolla näkyviin käyttäjän lainaushistoria
function MyLoanHistoryComponent(props) {

  const { myLoans, token, getBookName, getDueDate, books, refreshMyBooks, setRefreshMyBooks, setMyLoans, showReviewWindow } = props

  useEffect(() => {
    // console.log('token', token)
    getMyLoans()
  }, [setRefreshMyBooks, refreshMyBooks])

  const getMyLoans = async () => {
    const url = 'https://buutti-library-api.azurewebsites.net/api/loans/'
    const config = {
      headers: { "Authorization": "Bearer " + token }
    }
    const response = await axios.get(url, config)
    setMyLoans(response.data.reverse())
  }

  //palauttaa divin jossa oma lainaushistoria (kirjan nimi + palautuspäivä)
  return (
    <>
      <h2 className='MyLoanHeader'>My loaning history:</h2>
      <div className='MyLoans'>
        {myLoans.map(item => {
          if (item.returned !== null) {
            return (<p key={item.book_isbn}>
              <span className='loan'>{getBookName(item.book_isbn, books)}</span>
              <span className='date'>{`Returned: ${getDueDate(item.returned)}`}</span>
              <button className='reviewButton rightColumnBtn' onClick={() => showReviewWindow(item.book_isbn)}>Write a review</button>
            </p>)
          } else {
            return null
          }

        })}
      </div>
    </>
  )
}

// Sivun oikean reunan komponentti
function RightColumn(props) {
  const { refreshMyBooks, setRefreshMyBooks, isLoggedIn, token, books, username, myLoans, setMyLoans, readMyReviews, setReadMyReviews, setFees, setCurrentLoans, setTotalLoans, width } = props

  const [myReviews, setMyReviews] = useState([])
  const [writeReview, setWriteReview] = useState(false)
  const [loansOrHistory, setLoansOrHistory] = useState('loans')

  //tilamuuttujat joilla lähetetään tietyn kirjan tiedot arvostelun kirjoittamista varten
  const [isbn, setIsbn] = useState('')
  const [title, setTitle] = useState('')

  //funktio joka hakee kirjan nimen isbn:n avulla
  function getBookName(isbn) {
    for (let i = 0; i < props.books.length; i++) {
      if (props.books[i].isbn === isbn) {
        return props.books[i].title
      }
    }
  }

  //funktio joka muuttaa päivämäärän luettavampaan muotoon
  function getDueDate(date) {
    const newDate = new Date(date).toString()
    const gIndex = newDate.indexOf('G')
    const shortDate = newDate.slice(0, gIndex)
    return shortDate
  }

  //funktio joka palauttaa lainan
  const returnLoan = async (isbn) => {
    console.log('you returned a loan')
    const returnUrl = 'https://buutti-library-api.azurewebsites.net/api/loans/' + isbn
    const config = {
      headers: { "Authorization": "Bearer " + token }
    }
    const response = await axios.put(returnUrl, {}, config)
    setRefreshMyBooks(!refreshMyBooks)
  }

  //funktio joka pidentää laina-aikaa
  const extendLoan = async (isbn) => {
    const extendUrl = 'https://buutti-library-api.azurewebsites.net/api/loans/' + isbn
    const config = {
      headers: { "Authorization": "Bearer " + token }
    }
    const response = await axios.patch(extendUrl, {}, config)
    setRefreshMyBooks(!refreshMyBooks)
  }

  //funktio joka laittaa arvostelun kirjoitus -divin näkyviin ja asettaa kyseessä olevan kirjan tiedot tilamuuttujiin
  function showReviewWindow(isbn) {
    setWriteReview(true)
    setIsbn(isbn)
    const bookTitle = getBookName(isbn)
    setTitle(bookTitle)
  }

  //palauttaa oikeanpuoleisen kolumnin
  return (
    <div className='col-3 RightColumn' style={width < 1350 ? {  borderLeft: "5px solid #6A341D"} : {}}>
      {isLoggedIn === false ?
        <></>
        :
        <>
          <>
            <label> My loans
              <input type='radio' className='myLoansRadio' checked={loansOrHistory === 'loans'} onChange={() => setLoansOrHistory('loans')} />
            </label>
            <label> My loaning history
              <input type='radio' className='myLoansRadio' checked={loansOrHistory === 'history'} onChange={() => setLoansOrHistory('history')} />
            </label>
            {loansOrHistory === 'loans' ? <MyLoansComponent myLoans={myLoans} token={token} getBookName={getBookName} getDueDate={getDueDate} setMyLoans={setMyLoans} extendLoan={extendLoan} returnLoan={returnLoan} books={books} refreshMyBooks={refreshMyBooks} setRefreshMyBooks={setRefreshMyBooks} showReviewWindow={showReviewWindow} setCurrentLoans={setCurrentLoans} setTotalLoans={setTotalLoans} />
              :
              <MyLoanHistoryComponent myLoans={myLoans} token={token} setMyLoans={setMyLoans} getBookName={getBookName} getDueDate={getDueDate} books={books} refreshMyBooks={refreshMyBooks} setRefreshMyBooks={setRefreshMyBooks} showReviewWindow={showReviewWindow} setWriteReview={setWriteReview} />}

            {writeReview === true ? <ReviewWindow setWriteReview={setWriteReview} isbn={isbn} title={title} token={token} refreshMyBooks={refreshMyBooks} setRefreshMyBooks={setRefreshMyBooks} /> : null}
            {readMyReviews === true ? <MyReviews readMyReviews={readMyReviews} setReadMyReviews={setReadMyReviews} myReviews={myReviews} setMyReviews={setMyReviews} username={username} token={token} getBookName={getBookName} books={books} width={width}/> : null}
          </>
          <button className='BrowseMyReviewsButton' onClick={() => setReadMyReviews(true)}>Browse my reviews</button>
        </>
      }
    </div>

  )
}

export default RightColumn

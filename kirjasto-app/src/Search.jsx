import './Search.css'
import { GiMagnifyingGlass } from "react-icons/gi";
import { MdDeleteForever } from "react-icons/md";
import { useState, useEffect } from 'react';

function Search(props) {

    //Tilamuuttujassa käyttäjän syöte
    const [searchText, setSearchText] = useState('')

    //Jos kenttä pyyhitään tyhjäksi asetetaan counter ja filteredTeksti nollaan
    useEffect(() => {
        if(searchText === ''){
            clearSearch()
        }
      }, [searchText])


    const initialSearchText = 'Search by book title or book author...'

    function searchBooks(teksti) {
       if(teksti !== ''){
        //tämä funktio sitten etsii hakuehdon mukaiset kirjat tilamuuttujasta jossa kaikki kirjat
        props.setFilteredText(teksti)
        props.setCounter(0)
       }
    }

    //Funktio joka tyhjentää käyttäjän haun, searchTextin ja counterin
    function clearSearch(){
        props.setFilteredText('')
        setSearchText('')
        props.setCounter(0)
    }

    //Funktio joka kutsuu searchBooksia enterin painalluksesta
    const handleKeyDown = (event) => {
        if (event.key === 'Enter') {
         searchBooks(searchText)
        }
    }

    return (
    <div className='SearchContainer'>
        <input type='text'
        placeholder={initialSearchText}
        className='searchInput'
        value={searchText}
        onChange={(event) => 
            setSearchText(event.target.value)
        }
        onKeyDown={handleKeyDown}
        />
        <button className='searchButton' onClick={()=> searchBooks(searchText)}>
            <GiMagnifyingGlass className='searchIcon' /></button>
        <button className='searchButton' onClick={()=> clearSearch()}>
            <MdDeleteForever className='searchIcon'/></button>
    </div>
    )
}

export default Search
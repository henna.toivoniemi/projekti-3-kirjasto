import { useState } from 'react'
import './ShowBookReview.css'
import { useSound } from 'use-sound'
import pageturn1 from './assets/pageflip1.mp3'
import pageturn2 from './assets/pageflip2.mp3'
import { SlArrowLeft } from "react-icons/sl"
import { SlArrowRight } from "react-icons/sl"
import { IoClose } from "react-icons/io5"
import getStars from './getStars'

function ShowBookReview(props) {

  const [index, setIndex] = useState(0)
  const [playNext] = useSound(pageturn1)
  const [playPrevious] = useSound(pageturn2)
  const [focusRight, setFocusRight] = useState(true)
  const [focusLeft, setFocusLeft] = useState(false)

  let thisreviews = props.reviews.filter(review => review.book_isbn === props.showBookReviewIsbn)
  let reviews = thisreviews.length

  function decreaseIndex() {
    playPrevious()
    let newIndex = index - 1
    setIndex(newIndex)
  }

  function increaseIndex() {
    playNext()
    let newIndex = index + 1
    setIndex(newIndex)
  }

  // console.log('Index', index)
  // console.log('Reviews', reviews)

  // tarkistaa painetaanko nuolinäppäintä vasen/oikea 
  const handleKeyDown = (event) => {
    if (reviews > index + 1 && event.key === 'ArrowRight') {
      setFocusRight(true)
      setFocusLeft(false)
      increaseIndex()
    }
    if (index > 0 && event.key === 'ArrowLeft') {
      setFocusLeft(true)
      setFocusRight(false)
      decreaseIndex()
    }
  }

  const setAutoFocusRight = () => {
    if (reviews > index + 1) {
      return true
    } else {
      return false
    }
  }

const setAutoFocusLeft = () => {
  if (index > 0){
    return true
  } else {
    return false
  }
}

  return (
    <>
      <div className='grey' onClick={() => props.setBookReview('')} ></div>
      <div className='ShowBookReview' onKeyDown={handleKeyDown} >
        {props.books.map(book => book.isbn === props.showBookReviewIsbn
          ?
          <>
            <div className='ShowBookReviewTittle'><h2>{book.title}</h2></div>
            <div className='flexContainer'>
              <p className='browseButton'>{index > 0 ? <button autoFocus={setAutoFocusLeft} onClick={() => decreaseIndex()}><SlArrowLeft /></button> : <div className='flexFiller'></div>}</p>
              <div className='singleBookReview'>
                <p className='rating'>{getStars(thisreviews[index].rating)}</p>
                <p className='review'>{thisreviews[index].review}</p>
                <p className='reviewer'>{thisreviews[index].username}</p>
                <p className='filler'></p>
              </div>
              <p className='browseButton'>{reviews > index + 1 ? <button autoFocus={setAutoFocusRight} onClick={() => increaseIndex()} ><SlArrowRight /></button> : <div className='flexFiller'></div>} </p>
            </div>
          </>
          :
          <></>)}
        <p className='closeButton' onClick={() => props.setBookReview('')}><IoClose /></p>
      </div>
    </>
  )
}

export default ShowBookReview

//Komponentti joka laskee kaikki kirjat kyseisellä hakuehdoilla
function TotalBooksToShow(props) {
    
    //Navigoinnin viimeisellä sivulla näkyy counterin yläraja. Muuten counter + näkyvillä olevat kirjat
    return(
      props.counter + props.numberOfBooks > props.countBooksToShow() 
      ? 
      <p className="totalBooksCounter">{props.booksToShow} / {props.booksToShow}</p> 
      : 
      <p className="totalBooksCounter">{props.counter + props.numberOfBooks} / {props.booksToShow}</p>
    )
  }

  export default TotalBooksToShow
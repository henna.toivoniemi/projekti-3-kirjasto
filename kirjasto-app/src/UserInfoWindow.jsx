import './Banner.css'
import { GiExitDoor } from "react-icons/gi";

function UserInfoWindow({username, setUsername, password, setPassword, token, setToken, setIsLoggedIn, getRecommendation, fees, currentLoans, totalLoans, lateLoans, favoriteGenre, setBannerDisplay, setErrorMessage}) {

    const logout = () => {
        setToken('')
        setUsername('')
        setPassword('')
        setBannerDisplay(true)
        setIsLoggedIn(false)
        setErrorMessage('')
        localStorage.clear()
    }

    setToken(localStorage.getItem('token'))
    setUsername(localStorage.getItem('username'))
    setIsLoggedIn(true)


    return (
            <div className='Window2'>
                <div className='userHeader'>
                    <p>logged in as: <span style={{ fontWeight: 'bold', fontSize: '12pt' }}>{username}</span></p>
                </div>
                <div className='userInfo'>
                    <p className='currentLoans'>current loans: {currentLoans}</p>
                    <p className='totalLoans'>loans total: {totalLoans} </p>
                    {favoriteGenre !== '' ? <p className='favGenre'>your favorite genre: {favoriteGenre}</p> : <p className='favGenre'></p>}
                    <p className='loansLate' style={{ 'color': 'red' }}>loans late: {lateLoans}</p>
                    <p className='fees' style={{ 'color': 'red' }}>fees: {fees} €</p>
                    <button className='logOut' onClick={logout}><GiExitDoor className='searchIcon' /> Log out</button>
                </div>
            </div>
    )
}

export default UserInfoWindow
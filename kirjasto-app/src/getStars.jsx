import { IoMdStar } from "react-icons/io";
import { IoMdStarHalf } from "react-icons/io";

//Funktio joka hakee tähdet kirjan keskiarvon mukaan
  function getStars(average) {
    let stars
    average < 0.25 ? stars = <></> : null
    average >= 0.25 && average < 0.75 ? stars = <IoMdStarHalf /> : null
    average >= 0.75 && average < 1.25 ? stars = <IoMdStar /> : null
    average >= 1.25 && average < 1.75 ? stars = <><IoMdStar /><IoMdStarHalf /></> : null
    average >= 1.75 && average < 2.25 ? stars = <><IoMdStar /><IoMdStar /></> : null
    average >= 2.25 && average < 2.75 ? stars = <><IoMdStar /><IoMdStar /><IoMdStarHalf /></> : null
    average >= 2.75 && average < 3.25 ? stars = <><IoMdStar /><IoMdStar /><IoMdStar /></> : null
    average >= 3.25 && average < 3.75 ? stars = <><IoMdStar /><IoMdStar /><IoMdStar /><IoMdStarHalf /></> : null
    average >= 3.75 && average < 4.25 ? stars = <><IoMdStar /><IoMdStar /><IoMdStar /><IoMdStar /></> : null
    average >= 4.25 && average < 4.75 ? stars = <><IoMdStar /><IoMdStar /><IoMdStar /><IoMdStar /><IoMdStarHalf /></> : null
    average >= 4.75 ? stars = <><IoMdStar /><IoMdStar /><IoMdStar /><IoMdStar /><IoMdStar /></> : null
    return stars
  }

  export default getStars